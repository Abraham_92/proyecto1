﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vehiculo
{
    class Moto: Vehiculo_01
    {
        private int cap_pasajero;

        public Moto(String Marca, String Modelo,int Motor, int cap_pasajero):base(Marca,Modelo,Motor)
        {
            this.cap_pasajero = cap_pasajero;

        }

        public void pasajero_espacio(int pasajeros)
        {
            cap_pasajero = pasajeros;
        }

        public String dime_pasajero_cant()
        {
            return "Cantidad de pasajero por Moto es: "+ cap_pasajero;
        }
       
    }
}
