﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vehiculo
{
    class Vehiculo
    {
        static void Main(string[] args)
        {

            Vehiculo_01 Carro = new Vehiculo_01("Toyota","Corolla",4500);//objeto

           System.Console.WriteLine("\n "+Carro.DimeMarca()
               + "\n " + Carro.DimeMotor()
               + "\n " + Carro.dime_Modelo()+" \n");

            System.Console.WriteLine("************************* \n");

           Camion_Carga Camion = new Camion_Carga("Nissan","MK200", 8500, 5, 20);

            System.Console.WriteLine("\n " + Camion.DimeMarca()
               + "\n " + Camion.DimeMotor()
               + "\n " + Camion.dime_Modelo()
               + "\n " + Camion.dime_carga()
               + "\n " + Camion.dime_contenedor() + " \n");

            System.Console.WriteLine("************************* \n");

            Moto Motos_01 = new Moto("BMW", "MX200",7500, 2);

            System.Console.WriteLine("\n " + Motos_01.DimeMarca()
              + "\n " + Motos_01.DimeMotor()
              + "\n " + Motos_01.dime_Modelo()
              +"\n" + Motos_01.dime_pasajero_cant());


            string input = System.Console.ReadLine(); //Se utiliza para detener consola
        }
    }
}

class Vehiculo_01 //CLASE PADRE
{
    private String Marca;
    private String Modelo;
    private int ruedas;
    private int largo;
    private int ancho;
    private int Motor;
   

    public Vehiculo_01(String Marca, String Modelo, int Motor )
    {
       this.Marca = Marca;
       this.Modelo = Modelo;
       this.Motor = Motor;
    }

    public void MarcaCar_(String MarcaVehiculo)
    {
        Marca = MarcaVehiculo;
    }

    public String DimeMarca()
    {
        return "Marca del Vehiculo :" + Marca;

    }

    public void MotorCar( int Motor_Vel)
   {
        Motor = Motor_Vel;

    }

        public String DimeMotor()
    {
        return "Tiene un motor de " + Motor + "  centímetros cubicos";
    }

    public void modelo_car(String model_car)
    {
        Modelo = model_car;
    }
    public String dime_Modelo()
    {
        return "El modelo es: " +Modelo;
    }
}


