﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    class Blog
    {
        
         DateTime fechaCreacion;
         string titulo;
         string contenido;
         string[] etiquetas;
         int likes;
         bool liked;
         string[] comentarios;
         int numeroVisitas;

        public Blog (string _titulo, string _contenido, string [] _etiquetas)
        {
            this.titulo = _titulo;
            this.contenido = _contenido;
            this.etiquetas = _etiquetas;
        }

        public static void main(String [] args)
        {
            Blog news = new Blog("Politica","Deportan 525 indocumentado de EEUU","Lo mas Comun");
            System.Console.WriteLine("Television",news._titulo, news.contenido);

        }

        public DateTime FechaCreacion
        {
            get { return fechaCreacion; }
        }
        public string Titulo
        {
            get { return titulo; }
        }

        public string Contenido
        {
            get { return contenido; }
        }
        public string  [] Etiquetas
        {
            get
            {
                return etiquetas;
            }
        }
        public int Likes
        {
            get { return likes; }
            set { likes = value; }
        }
        public bool Liked
        {
            get { return liked; }
        }
        public string [] Comentarios
        {
            get { return comentarios; }
        }

        public int NumeroVisitas
        {
            get { return numeroVisitas; }
        }
    }
}


